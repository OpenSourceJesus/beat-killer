using UnityEngine;
using System;

namespace BeatKiller
{
	public class DebugManager : MonoBehaviour
	{
		public _Text textPrefab;
		public Transform textsParent;
		public int maxTextsCount;
		int textsCount;

		public void _Log (object o)
		{
			_Text text = Instantiate(textPrefab, textsParent);
			text.text.text = o.ToString();
			textsCount ++;
			if (textsCount > maxTextsCount)
			{
				DestroyImmediate(textsParent.GetChild(0).gameObject);
				textsCount --;
			}
		}

		public static void Log (object o)
		{
			GameManager.GetSingleton<DebugManager>()._Log (o);
		}
	}
}