﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using Unity.XR.Oculus.Input;

namespace BeatKiller
{
	public class Gun : MonoBehaviour, IUpdatable
	{
		public bool PauseWhileUnfocused
		{
			get
			{
				return false;
			}
		}
		public Transform trs;
		public Transform shootTrs;
		public Timer reloadTimer;
		[HideInInspector]
		public bool shootInput;
		[HideInInspector]
		public bool previousShootInput;
		bool isLeftGun;

		public virtual void OnEnable ()
		{
			// SetShootInput ();
			// previousShootInput = shootInput;
			isLeftGun = trs.parent == GameManager.GetSingleton<Player>().leftHandTrs;
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public virtual void DoUpdate ()
		{
			SetShootInput ();
			if (shootInput && !previousShootInput)
				Shoot ();
			previousShootInput = shootInput;
		}
		
		public virtual void Shoot ()
		{
		}

		void SetShootInput ()
		{
			if (isLeftGun)
			{
				InputManager.leftTouchController = (OculusTouchController) OculusTouchController.leftHand;
				DebugManager.Log (InputManager.leftTouchController == null);
				shootInput = InputManager.LeftTriggerPressedInput;
			}
			else
			{
				InputManager.rightTouchController = (OculusTouchController) OculusTouchController.rightHand;
				shootInput = InputManager.rightTouchController.trigger.ReadValue() > InputManager.Settings.defaultDeadzoneMin;
			}
		}

		public void FinishUsing ()
		{
			gameObject.SetActive(false);
			int actionQueueEntryIndex = (!isLeftGun).GetHashCode();
			GameManager.GetSingleton<ActionTable>().ChooseActionQueueEntry (actionQueueEntryIndex);
		}
		public virtual void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}