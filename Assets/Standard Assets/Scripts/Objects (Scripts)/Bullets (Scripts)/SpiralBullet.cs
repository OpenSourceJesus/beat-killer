using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace BeatKiller
{
	[ExecuteInEditMode]
	public class SpiralBullet : Bullet, IUpdatable
	{
		public bool PauseWhileUnfocused
		{
			get
			{
				return false;
			}
		}
		public Ray pullRay;
		public float pullSpeed;

		public override void OnEnable ()
		{
			GameManager.GetSingleton<ObjectPool>().RangeDespawn (prefabIndex, gameObject, trs, range);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			Vector3 currentPosition = trs.position;
			currentPosition += trs.forward * moveSpeed * Time.deltaTime;
			currentPosition += (pullRay.ClosestPoint(currentPosition) - trs.position).normalized * pullSpeed * Time.deltaTime;
			trs.position = currentPosition;
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}