using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeatKiller
{
	[ExecuteInEditMode]
	public class Bullet : Spawnable
	{
		public float moveSpeed;
		public Rigidbody rigid;
		public float range;
		[HideInInspector]
		public float rangeSqr;

		public virtual void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				rangeSqr = range * range;
				return;
			}
#endif
			GameManager.GetSingleton<ObjectPool>().RangeDespawn (prefabIndex, gameObject, trs, range);
			rigid.velocity = trs.forward * moveSpeed;
		}
	}
}