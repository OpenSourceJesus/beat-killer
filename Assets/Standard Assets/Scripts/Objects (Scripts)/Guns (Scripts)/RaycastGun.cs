using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeatKiller
{
	public class RaycastGun : Gun
	{
		public float damage;

		public override void Shoot ()
		{
			RaycastHit hit;
			if (Physics.Raycast(shootTrs.position, shootTrs.forward, out hit))
			{
				Note note = hit.collider.GetComponent<Note>();
				note.TakeDamage (damage);
			}
		}
	}
}