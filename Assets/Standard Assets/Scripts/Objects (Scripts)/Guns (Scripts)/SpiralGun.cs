using UnityEngine;
using System;
using Extensions;
using System.Collections.Generic;

namespace BeatKiller
{
	public class SpiralGun : Gun
	{
		public Phase phase;
		public float moveSpeed;
		public Ray pullRay;
		public float pullSpeed;
		public Transform pullRayLineRendererTrs;
		public LineRenderer shootTrajectoryLineRenderer;
		public SpiralBullet spiralBulletPrefab;

		public override void DoUpdate ()
		{
			base.DoUpdate ();
			if (!shootInput && previousShootInput)
				Shoot ();
			if (phase == Phase.AimAndShoot)
				HandleAimAndShoot ();
		}

		void HandleAimAndShoot ()
		{
			float distanceTraveled = 0;
			List<Vector3> positions = new List<Vector3>();
			Vector3 currentPosition = shootTrs.position;
			positions.Add(currentPosition);
			Vector3 previousPosition = currentPosition;
			do
			{
				currentPosition += shootTrs.forward * moveSpeed * Time.deltaTime;
				Vector3 position = pullRay.ClosestPoint(currentPosition);
				currentPosition += (position - currentPosition).normalized * pullSpeed * Time.deltaTime;
				distanceTraveled -= (currentPosition - previousPosition).sqrMagnitude;
				positions.Add(currentPosition);
				previousPosition = currentPosition;
			} while (distanceTraveled < spiralBulletPrefab.rangeSqr);
			shootTrajectoryLineRenderer.positionCount = positions.Count;
			shootTrajectoryLineRenderer.SetPositions(positions.ToArray());
		}

		public override void Shoot ()
		{
			if (phase == Phase.PlacePullRay)
			{
				pullRay = new Ray(shootTrs.position, shootTrs.forward);
				pullRayLineRendererTrs.SetParent(null);
				shootTrajectoryLineRenderer.gameObject.SetActive(true);
				phase = Phase.AimAndShoot;
			}
			else
				FinishUsing ();
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			Destroy(pullRayLineRendererTrs.gameObject);
		}

		public enum Phase
		{
			PlacePullRay,
			AimAndShoot
		}
	}
}