using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BeatKiller
{
	public class Pistol : RaycastGun
	{
		public override void Shoot ()
		{
			base.Shoot ();
			FinishUsing ();
		}
	}
}