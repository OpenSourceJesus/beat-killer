﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace BeatKiller
{
	public class Note : Spawnable
	{
		public float timeSpawned;
		public static List<Note> notes = new List<Note>();
		public int maxHp;
		public float hp;
		public Transform hpIconsParent;

		void OnEnable ()
		{
			hp = maxHp;
			for (int i = maxHp; i > 1; i --)
			{
				Transform hpIconTrs = Instantiate(hpIconsParent.GetChild(0), hpIconsParent);
				hpIconTrs.localScale = Vector3.one * (i - 1) / hp;
			}
			timeSpawned = Time.time;
			notes.Add(this);
			GameManager.GetSingleton<Survival>().noteCountText.text.text = "Targets: " + notes.Count;
			if (notes.Count > GameManager.GetSingleton<Survival>().maxNotes)
				GameManager.GetSingleton<Survival>().GameOver ();
		}
		
		public void TakeDamage (float amount = 1)
		{
			int previousHp = Mathf.RoundToInt(hp);
			hp -= amount;
			int destroyHpIconsCount = Mathf.Clamp(previousHp - Mathf.RoundToInt(hp), 0, hpIconsParent.childCount - 1);
			for (int i = 0; i < destroyHpIconsCount; i ++)
				DestroyImmediate(hpIconsParent.GetChild(0).gameObject);
			if (hp <= 0)
			{
				hpIconsParent.GetChild(0).localScale = Vector3.one;
				GameManager.GetSingleton<ObjectPool>().Despawn (prefabIndex, gameObject, trs);
				float scoreReward = GameManager.GetSingleton<Survival>().maxTimeToShootNote - (Time.time - timeSpawned);
				scoreReward = Mathf.Clamp(scoreReward, 0, Mathf.Infinity);
				GameManager.GetSingleton<Survival>().AddScore (scoreReward);
			}
		}

		void OnDisable ()
		{
			notes.Remove(this);
			GameManager.GetSingleton<Survival>().noteCountText.text.text = "Targets: " + notes.Count;
		}
	}
}