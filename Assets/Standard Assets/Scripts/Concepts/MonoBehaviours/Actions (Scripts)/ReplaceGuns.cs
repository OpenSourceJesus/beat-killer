using UnityEngine;

namespace BeatKiller
{
	public class ReplaceGuns : Action
	{
		public bool replaceLeftGun;
		public Gun leftGun;
		public bool replaceRightGun;
		public Gun rightGun;

		public override void Do ()
		{
			if (replaceLeftGun)
			{
				if (GameManager.GetSingleton<Player>().leftGun != null)
					Destroy(GameManager.GetSingleton<Player>().leftGun.gameObject);
				GameManager.GetSingleton<Player>().leftGun = Instantiate(leftGun);
				GameManager.GetSingleton<Player>().leftGun.gameObject.SetActive(true);
			}
			if (replaceLeftGun)
			{
				if (GameManager.GetSingleton<Player>().rightGun != null)
					Destroy(GameManager.GetSingleton<Player>().rightGun.gameObject);
				GameManager.GetSingleton<Player>().rightGun = Instantiate(rightGun);
				GameManager.GetSingleton<Player>().rightGun.gameObject.SetActive(true);
			}
		}
	}
}