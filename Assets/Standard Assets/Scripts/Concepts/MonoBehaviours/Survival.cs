﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using System;
using Random = UnityEngine.Random;

namespace BeatKiller
{
	public class Survival : MonoBehaviour
	{
		public Note notePrefab;
		public FloatRange spawnDistanceRange;
		public Timer spawnTimer;
		public float multiplySpawnTimerDuration;
		public float score;
		public _Text scoreText;
		public int Highscore
		{
			get
			{
				return PlayerPrefs.GetInt("Score", 0);
			}
			set
			{
				PlayerPrefs.SetInt("Score", value);
			}
		}
		public _Text highscoreText;
		public _Text noteCountText;
		public int maxTimeToShootNote;
		public int maxNotes;
		// bool improvedHighscore;
		
		void OnEnable ()
		{
			highscoreText.text.text = "Highscore: " + Highscore;
			spawnTimer.onFinished += SpawnNote;
			spawnTimer.Reset ();
			spawnTimer.Start ();
		}

		void SpawnNote (params object[] args)
		{
			Vector3 spawnPosition = Random.onUnitSphere * spawnDistanceRange.Get(Random.value);
			if (spawnPosition.z < 0)
				spawnPosition.z *= -1;
			GameManager.GetSingleton<ObjectPool>().SpawnComponent<Note>(notePrefab.prefabIndex, spawnPosition);
			spawnTimer.duration *= multiplySpawnTimerDuration;
			spawnTimer.Reset ();
			spawnTimer.Start ();
		}

		void OnDisable ()
		{
			spawnTimer.onFinished -= SpawnNote;
		}

		public void AddScore (float amount = 1)
		{
			score += amount;
			scoreText.text.text = "Score: " + (int) score;
			if ((int) score > Highscore)
			{
				Highscore = (int) score;
				highscoreText.text.text = "Highscore: " + Highscore;
				// improvedHighscore = true;
			}
		}

		public void GameOver ()
		{
			// if (improvedHighscore)
			// 	GameManager.GetSingleton<SaveAndLoadManager>().Save ();
			GameManager.GetSingleton<GameManager>().ReloadActiveScene ();
		}
	}
}