using UnityEngine;
using System;
using System.Collections.Generic;
using Extensions;
using Random = UnityEngine.Random;

namespace BeatKiller
{
	public class ActionTable : MonoBehaviour
	{
		public ActionQueueEntry[] actionQueueEntries = new ActionQueueEntry[0];
		public Action[] actionTypes = new Action[0];

		public void MoveToActionQueueEntry (ActionEntry actionEntry, ActionQueueEntry actionQueueEntry)
		{
			if (actionEntry.actionQueueEntryIndex != MathfExtensions.NULL_INT)
				actionEntry.ActionQueueEntry.actionEntries.RemoveAt(actionEntry.indexInActionQueueEntry);
			actionQueueEntry.actionEntries.Insert(actionEntry.indexInActionQueueEntry, actionEntry);
		}

		public void MoveToActionQueueEntry (ActionEntry actionEntry, int actionQueueEntryIndex)
		{
			MoveToActionQueueEntry (actionEntry, actionQueueEntries[actionQueueEntryIndex]);
		}

		public void ChooseActionQueueEntry (int actionQueueEntryIndex)
		{
			Action action = actionQueueEntries[actionQueueEntryIndex].actionEntries[0].action;
			action.Do ();
			actionQueueEntries[actionQueueEntryIndex].actionEntries.RemoveAt(0);
			AddToActionQueueEntry (actionQueueEntryIndex);
		}

		public void AddToActionQueueEntry (int actionQueueEntryIndex)
		{
			Action nextAction = GameManager.GetSingleton<ActionTable>().actionTypes[Random.Range(0, GameManager.GetSingleton<ActionTable>().actionTypes.Length)];
			GameManager.GetSingleton<ActionTable>().ChooseActionQueueEntry (actionQueueEntryIndex);
			GameManager.GetSingleton<ActionTable>().MoveToActionQueueEntry (new ActionTable.ActionEntry(nextAction), actionQueueEntryIndex);
		}

		[Serializable]
		public class ActionQueueEntry
		{
			public List<ActionEntry> actionEntries = new List<ActionEntry>();
		}

		[Serializable]
		public class ActionEntry
		{
			public ActionQueueEntry ActionQueueEntry
			{
				get
				{
					return GameManager.GetSingleton<ActionTable>().actionQueueEntries[actionQueueEntryIndex];
				}
			}
			public Action action;
			public int actionQueueEntryIndex = MathfExtensions.NULL_INT;
			public int indexInActionQueueEntry = MathfExtensions.NULL_INT;

			public ActionEntry (Action action)
			{
				this.action = action;
			}
		}
	}
}