using UnityEngine;
using Extensions;
using Unity.XR.Oculus.Input;

namespace BeatKiller
{
	public class Player : MonoBehaviour, IUpdatable
	{
		public bool PauseWhileUnfocused
		{
			get
			{
				return false;
			}
		}
		public Transform trs;
		public Transform leftHandTrs;
		public Transform rightHandTrs;
		public Gun leftGun;
		public Gun rightGun;
		bool leftTriggerInput;
		bool previousLeftTriggerInput;
		bool rightTriggerInput;
		bool previousRightTriggerInput;

		void OnEnable ()
		{
			previousLeftTriggerInput = InputManager.LeftTriggerPressedInput;
			previousRightTriggerInput = InputManager.rightTouchController.trigger.ReadValue() > InputManager.Settings.defaultDeadzoneMin;
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			InputManager.leftTouchController = (OculusTouchController) OculusTouchController.leftHand;
			// DebugManager.Log (InputManager.leftTouchController == null);
			leftHandTrs.position = InputManager.leftTouchController.devicePosition.ReadValue();
			leftHandTrs.rotation = InputManager.leftTouchController.deviceRotation.ReadValue();
			InputManager.rightTouchController = (OculusTouchController) OculusTouchController.rightHand;
			rightHandTrs.position = InputManager.rightTouchController.devicePosition.ReadValue();
			rightHandTrs.rotation = InputManager.rightTouchController.deviceRotation.ReadValue();
			if (leftGun == null)
			{
				leftTriggerInput = InputManager.LeftTriggerPressedInput;
				if (leftTriggerInput && !previousLeftTriggerInput)
					GameManager.GetSingleton<ActionTable>().ChooseActionQueueEntry (0);
				previousLeftTriggerInput = leftTriggerInput;
			}
			if (rightGun == null)
			{
				rightTriggerInput = InputManager.rightTouchController.trigger.ReadValue() > InputManager.Settings.defaultDeadzoneMin;
				if (rightTriggerInput && !previousRightTriggerInput)
					GameManager.GetSingleton<ActionTable>().ChooseActionQueueEntry (1);
				previousRightTriggerInput = rightTriggerInput;
			}
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}