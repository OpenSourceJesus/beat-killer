using UnityEngine;

namespace Extensions
{
	public static class RayExtensions
	{
		public static Vector3 ClosestPoint (this Ray ray, Vector3 point)
		{
			Plane plane = new Plane(ray.direction, ray.origin);
			plane.distance = plane.GetDistanceToPoint(point);
			return ray.GetPoint(plane.distance);
		}
	}
}